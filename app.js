var express = require('express'),
    app = express(),
    engines = require('consolidate'),
    bodyParser = require('body-parser'),
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
app.use(bodyParser());
app.engine('html', engines.nunjucks);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.use(bodyParser.urlencoded({ extended: true }));

// Handler for internal server errors
function errorHandler(err, req, res, next) {
    console.error(err.message);
    console.error(err.stack);
    res.status(500).render('error_template', { error: err });
}

MongoClient.connect('mongodb://agendaki:canario456@ds019491.mlab.com:19491/agendaki', function(err, db) {

    assert.equal(null, err);
    console.log("Successfully connected to MongoDB.");

    app.get('/', function(req, res, next) {

        db.collection('filmes').find({}).toArray(function(err, filme) {
            res.render('cadastro-filme', { 'filmes': filme });
        });
    });

    app.get('/cadastro-usuario', function(req, res, next) {

        db.collection('usuarios').find({}).toArray(function(err, usuario) {
            res.render('cadastro-usuario', { 'usuarios': usuario });
        });
    });

    app.get('/classificacao-filme', function(req, res, next) {

        db.collection('usuarios').find({}).toArray(function(err, usuario) {
            res.render('cadastro-usuario', { 'usuarios': usuario });
        });
    });

    app.post('/cadastro-filme', function(req, res, next) {
        assert.equal(null, err);
        var titulo = req.body.titulo,
            genero = req.body.genero,
            ano = req.body.ano

        db.collection('filmes').insert({
            titulo: titulo,
            ano: ano,
            genero: genero,
            avaliado: false
        }, function(err, docs) {});
    });

    app.post('/cadastro-usuario', function(req, res, next) {
        assert.equal(null, err);
        var nome = req.body.nome,
            idade = req.body.idade,
            escolaridade = req.body.escolaridade,
            sexo = req.body.sexo,
            generos_preferidos = req.body.generos_preferidos,
            salario = req.body.salario

        db.collection('usuarios').insert({
            nome: nome,
            idade: idade,
            escolaridade: escolaridade,
            sexo: sexo,
            generos_preferidos: generos_preferidos,
            salario: salario
        }, function(err, docs) {});
    });


    app.use(errorHandler);

    app.use(function(req, res) {
        res.sendStatus(404);
    });

    var server = app.listen(3000, function() {
        var port = server.address().port;
        console.log('Express server listening on port %s.', port);
    });
});
