# Banco de dados Atividade MongoDB – Sistema para Sugestão de Filmes Utilizando MongoDB

### Desenvolver um sistema utilizando o MongoDB para sugestão de filmes com base no perfil dos usuários e características dos filmes.

## Requisitos funcionais:
- RF01: O sistema deve permitir cadastrar filmes.
- RF02: O sistema deve permitir cadastrar usuários.
- RF03: O sistema deve permitir informar a avaliação dos filmes pelos usuários.
- RF04: O sistema deve permitir apresentar quais os filmes mais se assemelham aos gostos de cada usuários.

## Regras de negócio:
- RN01: Um filme é avaliado através de uma nota entre 0 e 5 (pode-se utilizar componentes de seleção de estrelas).
- RN02: Um usuário não pode avaliar mais de uma vez o mesmo filme.

#### Sugestões: • Não se preocupe em relação ao login do usuário ou elegância da interface. 

#### A avaliação de filmes pode ser feita através dos seguintes passos:
- ◦ Seleção de um usuário em uma lista de usuários
- ◦Seleção de um filme em uma lista de filmes
- ◦Informe da nota do usuário selecionado para o filme selecionado
• Utilize a criatividade para elencar quais atributos dos filmes e dos usuários serão armazenados.

A escolha dos atributos tem relação direta com a forma do cálculo de sugestão.
◦ Exemplo de atributos interessantes de filmes: Gênero, faixa etária, palavras-chave, atores.
◦ Exemplo de atributos interessantes de usuários:
Escolaridade,
Faixa de renda,
Estilo musical,
Gêneros preferidos.

### O que será avaliado:
• Forma da estruturação das coleções;
• Manipulação e consultas dos dados;
• Criatividade na solução proposta.

### O que não será avaliado:

- • Elegância da interface;
- • Aspectos de usabilidade.
- • Tecnologias / frameworks escolhidos (não é permitido o uso de camadas de abstração de banco de dados)